<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')ON#%dh@VDbT,-;=wf)Kp:uGyb~]}z_F;1D&J1B  |w3_t!o*CJ8/R6-v3tX)sgy');
define('SECURE_AUTH_KEY',  'Fs~|5UX5[C3dqs*gw=f=F`+J[ohz?=iy8MEUu[cHLJ3$vd,q*[&{irs82Qaki.;B');
define('LOGGED_IN_KEY',    'S|77%Z;sJDR~kO3L`aA,,{2f]Qs~I_egr]56nUHj:p+810Ia($4haauj5)*&sy/$');
define('NONCE_KEY',        'J<O5pa[J?)n9}%VAvgLMl4`OoA--s^m4=P ~2V,ok+Nl&fH#]R$,AH?=?{cK=c>5');
define('AUTH_SALT',        '70rE%kp}J=-||/PoqW6)3Q2}Q_[OID<0:BY0Z%vU8lZ2+iW&8ZY5kVK4]t5n1:;k');
define('SECURE_AUTH_SALT', '<};OTq$8E7O+>r)3<kn?:5zrkW#=s*;,ba~zpUUGP^k84UhdpD+}TCN4t9I02F4r');
define('LOGGED_IN_SALT',   'R:cg-J6,ZW(8.L},5WI?lip?o~wx0K=>(v=-Q&@=aGB^U|-6#)!3nG8+{labxdEa');
define('NONCE_SALT',       'h?>($j8!rQ/6 $ZB0I$#0D%hN1K!/r?EX=M94Y[fK2OnHhAH^Y`0a)BV;k:9oxl-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp4_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
