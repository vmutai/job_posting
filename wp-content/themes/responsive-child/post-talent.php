<?php 
 
/**
 * Template Name: Post Talent Page
 */

acf_form_head();

get_header(); 
 
the_post(); 
 
?>
 
<div id="primary">
    <div id="content" role="main">

        <?php 

        $args = array(
            'post_id' => 'new-talent',
            'field_groups' => array( 1, 2, 3, 4 ),
            'submit_value' => 'Submit', // value for submit field
            'updated_message' => 'Job submitted. You can post a different job below.', // default updated message. Can be false to show no message
        );

        acf_form( $args ); 

        ?>

    </div><!-- #content -->
</div><!-- #primary -->
