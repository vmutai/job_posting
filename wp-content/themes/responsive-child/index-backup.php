<?php 
 
/**
 * Template Name: Page with ACF form
 */
 
acf_form_head();
 
get_header(); 
 
the_post(); 

$options = array(
    'post_id' => 'new', // post id to get field groups from and save data to
    'field_groups' => array(), // this will find the field groups for this post (post ID's of the acf post objects)
    'form' => true, // set this to false to prevent the <form> tag from being created
    'form_attributes' => array( // attributes will be added to the form element
        'id' => 'post',
        'class' => '',
        'action' => '',
        'method' => 'post',
    ),
    'return' => add_query_arg( 'updated', 'true', get_permalink() ), // return url
    'html_before_fields' => '', // html inside form before fields
    'html_after_fields' => '', // html inside form after fields
    'submit_value' => 'Submit', // value for submit field
    'updated_message' => 'Post updated.', // default updated message. Can be false to show no message
);
 
?>

<?php 
	$args = array('posts_per_page' => 6, 'post_type' => 'jobs');
	$query = new WP_Query($args);
	while($query->have_posts()) : $query->the_post();
?>
	<div id="primary">
		<div id="content" role="main">
 
			<?php 
 
			$args = array( $options );
 
			acf_form( $args ); 
 
			?>
 
		</div><!-- #content -->
	</div><!-- #primary -->


<?php 
	endwhile;
?>