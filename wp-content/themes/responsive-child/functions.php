<?php
	// start create custom forms
	function create_post_type() {
		register_post_type('jobs', 
			array('labels'=> 
				array(
					'name' => __("Jobs"), // __ means a word to be translated. find localized translation
					'singular_name' => __("Job")
				),
				'public' => true,
				'menu_position' => 5,
				'supports' => array('title','editor','author','thumbnail','excerpt','comments', 'page-attributes', 'post-formats'),
				'taxonomies' => array('post_tag', 'categories', 'industry'),
				'rewrite' => array('slug' => 'jobs') // mask our slug in something more appealling. it is gonna show awefulmedia_games as games
			)
		);

		register_post_type('talents', 
			array('labels'=> 
				array(
					'name' => __("Talents"), // __ means a word to be translated. find localized translation
					'singular_name' => __("Talent")
				),
				'public' => true,
				'menu_position' => 5,
				'supports' => array('title','editor','author','thumbnail','excerpt','comments', 'page-attributes', 'post-formats'),
				'taxonomies' => array('post_tag', 'categories', 'industry'),
				'rewrite' => array('slug' => 'talents') // mask our slug in something more appealling. it is gonna show awefulmedia_games as games
			)
		);

		register_post_type('companies',
			
		);
	}

	add_action('init', 'create_post_type');

	 
	function my_pre_save_post( $post_id )
	{
	    // check if this is to be a new post
	    if( $post_id != 'new_3' )
	    {
	        return $post_id;
	    }

	    // Create a new post
	    $post = array(
	        'post_status'  => 'draft' ,
	        'post_title'  => $_POST['fields']["field_532a079249dd5"], //'title', //$_POST['fields']['title'],
	        'post_type'  => 'jobs',
	    );  
	 
	    // insert the post
	    $post_id = wp_insert_post( $post ); 
	 
	    // update $_POST['return']
	    $_POST['return'] = add_query_arg( array('post_id' => $post_id), $_POST['return'] );    

	 
	    // return the new ID
	    return $post_id;
	}
	 
	add_filter('acf/pre_save_post' , 'my_pre_save_post' );
 
?>
