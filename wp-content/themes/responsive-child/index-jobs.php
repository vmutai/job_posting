
<div id="content">
<?php
 
/**
 * Template Name:  Job Listing
 */
 
acf_form_head();
 
get_header(); ?>

<div id="primary">
        <div id="content" role="main">

        <?php 
            $args = array('posts_per_page' => 6, 'post_type' => 'jobs');
            $query = new WP_Query($args);
            while($query->have_posts()) : $query->the_post();
        ?>
        <?php get_template_part('job-section'); ?>
        <?php 
            endwhile;
        ?>

        </div><!-- #content -->
    </div><!-- #primary -->

</div><!-- end of #content -->